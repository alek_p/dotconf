# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# Add onbld to the path
if [ -e /opt/onbld/bin ]; then
	export PATH="$PATH:/opt/onbld/bin"
fi

# pkgin
if [ -e /opt/pkg/bin ]; then
	export PATH="$PATH:/opt/pkg/bin"
fi

# Set my editor and git editor
export EDITOR="vim"
export GIT_EDITOR="vim"

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
#export HISTCONTROL=ignoredups

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" -a -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
#case "$TERM" in
#xterm-color)
#    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
#    ;;
#*)
#    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
#    ;;
#esac

# Comment in the above and uncomment this below for a color prompt
PS1="\\[${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;35m\]\w\[\033[00m\]\$ "

# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME}: ${PWD/$HOME/~}\007"'
    ;;
*)
    ;;
esac

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

#if [ -f ~/.bash_aliases ]; then
#    . ~/.bash_aliases
#fi

# enable color support to some commands and add other handy aliases
if [ "$TERM" != "dumb" ]; then
    eval "`dircolors -b`"
    alias ls='ls --color=auto'
    alias hf='history | \/bin/grep --color=always'
    alias fn='find . -name'
    alias rgrep='/bin/grep -r -n --color=always'
    alias grep='/bin/grep -n --color=always'
    alias less='less -R'
    alias diff='diff -u'

    #alias dir='ls --color=auto --format=vertical'
    #alias vdir='ls --color=auto --format=long'
fi

# some more ls aliases
#alias ll='ls -l'
#alias la='ls -A'
#alias l='ls -CF'

# dir and exec color
LS_COLORS='ex=0;32:di=0;35' ; export LS_COLORS

# disable control flow (ctrl+s) enable SW controll flow
stty -ixon
# map ctrl+s to nothing
bind "\C-s":"" #reverse-search-history

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
#if [ -f /etc/bash_completion ]; then
#    . /etc/bash_completion
#fi
HISTTIMEFORMAT="%h %d %H:%M:%S "

HISTFILESIZE=10000
HISTSIZE=10000

# add un/autotop cmd to always keep prompt at top of screen
# https://github.com/swirepe/alwaysontop/blob/master/alwaysontop.sh
COLOR_off='\033[0m'
COLOR_BIPurple='\033[1;95m'
COLOR_BIYellow='\033[1;93m'
COLOR_IBlack='\033[0;90m'
COLOR_BGreen='\033[1;32m'
COLOR_BRed='\033[1;31m'

function _gototop_bash {
    # go to the top of the screen and clear in both directions
    # zsh seems to have very strong opinions about redrawing all of the screen
    # when this is called
    tput cup 0 0
    tput el
    tput el1
}

function autoclear {
    if [[ "$AUTOCLEAR" != "TRUE" ]]
    then
        export AUTOCLEAR="TRUE"
        if [[ "$SHELL" == *"bash"* ]]
        then
                bind 'RETURN: "\C-l\C-j"'
        elif [[ "$SHELL" == *"screen"* ]]
        then
                bind 'RETURN: "\C-a \C"'
        fi
    fi

    echo -e "[alwaysontop.sh] ${COLOR_BIYellow}autoclear${COLOR_off} ${COLOR_BGreen}ON${COLOR_off}."
}

function unautoclear {
    export AUTOCLEAR="FALSE"

    if [[ "$SHELL" == *"bash"* ]]
    then
        bind 'RETURN: "\C-j"'
    elif [[ "$SHELL" == *"screen"* ]]
    then
        bind 'RETURN: "\C-j"'
    fi

    echo -e "[alwaysontop.sh] ${COLOR_BIYellow}autoclear${COLOR_off} ${COLOR_BRed}OFF${COLOR_off}."
}

function alwaysontop {
    if [[ "$ALWAYSONTOP" != "TRUE" ]]
    then
            export ALWAYSONTOP="TRUE"
            export OLD_PROMPT_COMMAND_AOT="$PROMPT_COMMAND"
            if [ "$PROMPT_COMMAND" ]
            then
                # before showing the prompt and doing whatever you're supposed to do
                # when you do that, go to the top of the screen and clear in both directions
                PROMPT_COMMAND="$PROMPT_COMMAND ; _gototop_bash"
                PROMPT_COMMAND=$(echo $PROMPT_COMMAND | sed -e 's/;\s*;/;/')
            else
                PROMPT_COMMAND=" _gototop_bash "
            fi
    fi

    echo -e "[alwaysontop.sh] ${COLOR_BIPurple}always on top${COLOR_off} ${COLOR_BGreen}ON${COLOR_off}."
}

function unalwaysontop {
    if [[ "$ALWAYSONTOP" == "TRUE"  ]]
    then
            if [ -n $OLD_PRMOPT_COMMAND_AOT ]
            then
                PROMPT_COMMAND="$OLD_PRMOPT_COMMAND_AOT"
            fi
            ALWAYSONTOP="FALSE"
    fi

    echo -e "[alwaysontop.sh] ${COLOR_BIPurple}always on top${COLOR_off} ${COLOR_BRed}OFF${COLOR_off}."
}

# turn on both alwaysontop and autoclear
function autotop {
    clear
    autoclear
    alwaysontop
}

# turn off both alwaysontop and autoclear
function unautotop {
    unalwaysontop
    unautoclear
}

