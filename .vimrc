" check if vim was compiled w/ cscope support enabled
if has("cscope")
	" set which cscope to use
	if filereadable("/opt/onbld/bin/i386/cscope-fast")
		set cscopeprg=/opt/onbld/bin/i386/cscope-fast
	endif
	"          cscope tuning
	" ----------------------------------
	if has("quickfix")
		set cscopequickfix=s-,c-,d-,i-,t-,e-
	endif
	"set cscopeverbose
	set cscopetag
	set exrc
	set csto=0
	set cst
	silent! map <unique> <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
	silent! map <unique> <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
	silent! map <unique> <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>
	silent! map <unique> <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
	silent! map <unique> <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
	silent! map <unique> <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
	silent! map <unique> <C-\>f :cs find f <C-R>=expand("<cword>")<CR><CR>
	silent! map <unique> <C-\>i :cs find i <C-R>=expand("<cword>")<CR><CR>
endif

"       restore position in file
" -----------------------------------
" Restore cursor position to where it was before
augroup JumpCursorOnEdit
   au!
   autocmd BufReadPost *
            \ if expand("<afile>:p:h") !=? $TEMP |
            \   if line("'\"") > 1 && line("'\"") <= line("$") |
            \     let JumpCursorOnEdit_foo = line("'\"") |
            \     let b:doopenfold = 1 |
            \     if (foldlevel(JumpCursorOnEdit_foo) > foldlevel(JumpCursorOnEdit_foo - 1)) |
            \        let JumpCursorOnEdit_foo = JumpCursorOnEdit_foo - 1 |
            \        let b:doopenfold = 2 |
            \     endif |
            \     exe JumpCursorOnEdit_foo |
            \   endif |
            \ endif
   " Need to postpone using "zv" until after reading the modelines.
   autocmd BufWinEnter *
            \ if exists("b:doopenfold") |
            \   exe "normal zv" |
            \   if (b:doopenfold > 1) |
            \       exe  "+".1 |
            \   endif |
            \   unlet b:doopenfold |
            \ endif
augroup END

"set viminfo='10,\"100,:20,%,n~/.viminfo
"au BufReadPost * if line("'\"") > 0|if line("'\"") <= line("$")|exe("norm '\"")|else|exe "norm $"|endif|endif

"      kernel coding style
" -----------------------------------
set formatoptions=croql
set cino=t0,:0,(0,)100,*100
set cindent sm wm=0 tw=0
set comments=sr:/*,mb:*,ex:*/,://
let c_space_errors=1

" Enable mouse support in console for scrolling w/ wheel
" -------------------------------------------------------
set mouse=nicr " use Shift + mouse click to copy paste
set ttymouse=xterm2 " makes it work in everything
" toggle between terminal and vim mouse
" Only do this when not done yet for this buffer
if exists("b:loaded_toggle_mouse_plugin")
    finish
endif
let b:loaded_toggle_mouse_plugin = 1

fun! s:ToggleMouse()
    if !exists("s:old_mouse")
        let s:old_mouse = "a"
    endif

    if &mouse == ""
        let &mouse = s:old_mouse
        echo "Mouse is for Vim (" . &mouse . ")"
    else
        let s:old_mouse = &mouse
        let &mouse=""
        echo "Mouse is for terminal"
    endif
endfunction

" Add mappings, unless the user didn't want this.
" The default mapping is registered under to <F12> by default, unless the user
" remapped it already (or a mapping exists already for <F12>)
if !exists("no_plugin_maps") && !exists("no_toggle_mouse_maps")
    if !hasmapto('<SID>ToggleMouse()')
        noremap <F12> :call <SID>ToggleMouse()<CR>
        inoremap <F12> <Esc>:call <SID>ToggleMouse()<CR>a
    endif
endif

" Highlight things that we find with the search
set hlsearch

" jump to partial search match
set incsearch

" This shows what you are typing as a command.
set showcmd

" Use english for spellchecking, but don't spellcheck by default
if version >= 700
   set spl=en spell
   set nospell
endif

" Show trailing whitepace and lone tabs:
:highlight ExtraWhitespace ctermbg=red guibg=red
:match ExtraWhitespace /\s\+$/


"        spaces instead of tabs
" -----------------------------------
"set ts=4
"set sts=4
"set et

" no auto identation (for languages)
" -----------------------------------
set noai

" make backspace work like like most other apps
" ---------------------------------------------
set backspace=indent,eol,start

" Remove any trailing whitespace that is in the file
"autocmd BufRead,BufWrite * if ! &bin | silent! %s/\s\+$//ge | endif

" make searches case-sensitive only if they contain upper-case characters
set ignorecase smartcase

" visual help stuff
" -----------------
syntax on
set showmatch  " for bracets
set laststatus=2 "show status line
set statusline=%F%m%r%h%w\ [%L]\ [%{&ff}]\ %y%=[%p%%]\ [line:%l,col:%v]
set scrolloff=3  " min lines to keep aroung cursor
"set number      "show all line numbers

" Highlighting
" -----------------------------------------------------------------
highlight Comment         ctermfg=DarkGrey guifg=#444444
highlight StatusLineNC    ctermfg=Black ctermbg=DarkGrey cterm=bold
highlight StatusLine      ctermbg=Black ctermfg=LightGrey

" in insert mode use F5 to enter paste mode
" -----------------------------------------
set pastetoggle=<F5>

set background=dark

" In insert mode show vertical line at char #80
" ---------------------------------------------
"set colorcolumn=80
augroup ColorcolumnOnlyInInsertMode
  autocmd!
  autocmd InsertEnter * setlocal colorcolumn=80
  autocmd InsertLeave * setlocal colorcolumn=0
augroup END
" vertical line color
" ----------------------------------------
:hi ColorColumn ctermbg=blue guibg=blue
" highlight char at pos 80
" ----------------------------------------
:match ErrorMsg '\%>80v.\+'

