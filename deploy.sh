#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function cp_file {
	if [ -f $2 ]; then
		if [ ! -f $2.orig ]; then
			echo $2 "found, copying it to" $2.orig
			cp $2 $2.orig
		fi
	fi

	echo "Deploying $2";
	cp $1 $2
}

function deploy_file {
	cp_file $DIR/$1 ~/$1
}

deploy_file .screenrc
deploy_file .vimrc
deploy_file .bashrc
deploy_file .bash_profile
deploy_file .gitconfig

exit
